/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type CreateData struct {
	Type  uint64          `json:"type"`
	Value json.RawMessage `json:"value"`
}
