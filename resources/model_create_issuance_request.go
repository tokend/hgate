/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type CreateIssuanceRequest struct {
	// Tasks to create request with
	AllTasks *uint32 `json:"all_tasks,omitempty"`
	Amount   Amount  `json:"amount"`
	// Unique asset code
	Asset          string   `json:"asset"`
	CreatorDetails *Details `json:"creator_details,omitempty"`
	// Issuance reveiver balance
	Receiver string `json:"receiver"`
	// Optional inique reference. If ommited random string will be used.
	Reference *string `json:"reference,omitempty"`
}
