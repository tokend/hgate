module github.com/tokend/hgate

go 1.16

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/certifi/gocertifi v0.0.0-20210507211836-431795d63e8d // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/google/jsonapi v1.0.0 // indirect
	github.com/nullstyle/go-xdr v0.0.0-20180726165426-f4c839f75077 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/urfave/cli v1.22.5
	gitlab.com/distributed_lab/ape v1.5.0
	gitlab.com/distributed_lab/figure v2.1.0+incompatible
	gitlab.com/distributed_lab/json-api-connector v0.2.4 // indirect
	gitlab.com/distributed_lab/kit v1.8.6
	gitlab.com/distributed_lab/logan v3.8.0+incompatible
	gitlab.com/distributed_lab/lorem v0.2.0 // indirect
	gitlab.com/tokend/connectors v0.1.8
	gitlab.com/tokend/go v3.15.0+incompatible
	gitlab.com/tokend/keypair v0.0.0-20190412110653-b9d7e0c8b312
	gitlab.com/tokend/regources v4.9.1+incompatible
)
