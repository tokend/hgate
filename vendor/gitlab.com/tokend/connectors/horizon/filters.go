package horizon

import (
	"fmt"
	"net/url"
)

type Query struct {
	Field string
	Value interface{}
}

func (q Query) String() string {
	return fmt.Sprintf("%s=%v", q.Field, q.Value)
}

func (q Query) StringValue() string {
	return fmt.Sprintf("%v", q.Value)
}

type Queries []Query

func (q Queries) Encode() string {
	u := url.Values{}

	for _, query := range q {
		u.Add(query.Field, query.StringValue())
	}

	return u.Encode()
}

func GetFilterAccount(v string) Query {
	return Query{
		Field: "filter[account]",
		Value: v,
	}
}

func GetFilterRole(v string) Query {
	return Query{
		Field: "filter[role]",
		Value: v,
	}
}

func GetFilterOwner(v string) Query {
	return Query{
		Field: "filter[owner]",
		Value: v,
	}
}

func GetFilterState(v string) Query {
	return Query{
		Field: "filter[state]",
		Value: v,
	}
}

func GetFilterAsset(v string) Query {
	return Query{
		Field: "filter[asset]",
		Value: v,
	}
}

func GetFilterAssetOwner(v string) Query {
	return Query{
		Field: "filter[asset_owner]",
		Value: v,
	}
}

func GetFilterBaseAsset(v string) Query {
	return Query{
		Field: "filter[base_asset]",
		Value: v,
	}
}

func GetFilterQuoteAsset(v string) Query {
	return Query{
		Field: "filter[quote_asset]",
		Value: v,
	}
}

func GetFilterRequestor(v string) Query {
	return Query{
		Field: "filter[requestor]",
		Value: v,
	}
}

func GetFilterReviewer(v string) Query {
	return Query{
		Field: "filter[reviewer]",
		Value: v,
	}
}

func GetFilterType(v string) Query {
	return Query{
		Field: "filter[type]",
		Value: v,
	}
}

func GetInclude(v string) Query {
	return Query{
		Field: "include",
		Value: v,
	}
}
