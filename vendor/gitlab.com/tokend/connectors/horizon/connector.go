package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/keyvalue"
	"gitlab.com/tokend/connectors/lazyinfo"
	"gitlab.com/tokend/connectors/request"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/connectors/submit"
	"gitlab.com/tokend/keypair"
	"net/url"
)

type Connector struct {
	*horizon.Connector
	*submit.Submitter
	*lazyinfo.LazyInfoer
	*keyvalue.KeyValuer
	*request.Reviewer
	Client *signed.Client
	Signer keypair.Full
}

func NewConnector(cli *signed.Client, signer keypair.Full) *Connector {
	return &Connector{
		Client:     cli,
		Submitter:  submit.New(cli),
		Connector:  horizon.NewConnector(cli),
		KeyValuer:  keyvalue.New(cli),
		LazyInfoer: lazyinfo.New(cli),
		Reviewer: request.New(cli, keyer.Keys{
			Signer: signer,
			Source: signer,
		}),
		Signer: signer,
	}
}

func (c *Connector) GetList(url string, response interface{}, query ...Query) error {
	err := horizon.NewStreamer(c.Client, url, Queries(query)).Next(response)

	if err != nil {
		if err == horizon.ErrNotFound || err == horizon.ErrDataEmpty {
			return nil
		}
		return errors.Wrap(err, "failed to get data list")
	}

	return nil
}

func (c *Connector) GetOne(p Path, response interface{}, query ...Query) error {
	path, err := url.Parse(p.Path())
	if err != nil {
		return errors.Wrap(err, "failed to parse url")
	}

	for _, q := range query {
		path.Query().Add(q.Field, q.StringValue())
	}

	err = c.Get(path, response)
	if err != nil {
		if err == horizon.ErrNotFound {
			return nil
		}
		return err
	}

	return nil
}
