package request

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/keypair/figurekeypair"
	"net/http"
	"net/url"
)

type Reviewerer interface {
	Reviewer() *Reviewer
}

type reviewerer struct {
	getter kv.Getter
	once   comfig.Once
	keyer.Keyer
}

func NewReviewerer(getter kv.Getter) Reviewerer {
	return &reviewerer{
		getter: getter,
		Keyer:  keyer.NewKeyer(getter),
	}
}

func (r *reviewerer) Reviewer() *Reviewer {
	return r.once.Do(func() interface{} {
		keys := r.Keyer.Keys()

		var config struct {
			Endpoint *url.URL `fig:"endpoint,required"`
		}

		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(r.getter, "reviewer")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out submit"))
		}

		cli := signed.NewClient(http.DefaultClient, config.Endpoint)
		if keys.Signer != nil {
			cli = cli.WithSigner(keys.Signer)
		}

		return New(cli, keys)
	}).(*Reviewer)
}
