package horizon

import (
	"gitlab.com/tokend/connectors/horizon"
	"gitlab.com/tokend/connectors/signed"
)

type Connector struct {
	*horizon.Connector
}

func NewConnector(client *signed.Client) *Connector {
	return &Connector{
		Connector: horizon.NewConnector(client, nil),
	}
}
