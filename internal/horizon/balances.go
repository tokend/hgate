package horizon

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"
	regources "gitlab.com/tokend/regources/generated"
)

func (c *Connector) GetDefaultBalanceID(asset string, accountId string) (string, error) {
	var balancesList regources.BalanceListResponse
	endpoint, err := url.Parse(fmt.Sprintf("v3/balances?filter[asset]=%s&filter[owner]=%s", asset, accountId))
	if err != nil {
		panic(err)
	}
	if err := c.Get(endpoint, &balancesList); err != nil {
		return "", errors.Wrap(err, "failed to get user default balance")
	}
	if len(balancesList.Data) == 0 {
		return "", errors.Wrapf(err, "balance for account id %s and asset %s does not exist", accountId, asset)
	}

	return balancesList.Data[0].ID, nil
}
