package horizon

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"

	"github.com/tokend/hgate/resources"
)

func (c *Connector) GetAccountIDByIdentifier(identifier string) (string, error) {
	var identityList IdentityListResponse
	endpoint, err := url.Parse(fmt.Sprintf("identities?filter[identifier]=%s", identifier))
	if err != nil {
		panic(err)
	}
	if err := c.Get(endpoint, &identityList); err != nil {
		return "", errors.Wrap(err, "failed to get user identity")
	}
	if len(identityList.Data) == 0 {
		return "", errors.Wrapf(err, "identity for identifier %s not found", identifier)
	}

	return identityList.Data[0].Attributes.Address, nil
}

type IdentityListResponse struct {
	Data     []Identity         `json:"data"`
	Included resources.Included `json:"included"`
	Links    *resources.Links   `json:"links"`
}

type Identity struct {
	resources.Key
	Attributes    IdentityAttributes    `json:"attributes"`
	Relationships IdentityRelationships `json:"relationships"`
}

type IdentityAttributes struct {
	// unique identifier of the user account
	Address string `json:"address"`
	// email address
	Email string `json:"email"`
	// user's phone number
	PhoneNumber *string `json:"phone_number,omitempty"`
	// user's status
	Status string `json:"status"`
	// user's telegram username
	TelegramUsername *string `json:"telegram_username,omitempty"`
	UpdatedAt        string  `json:"updated_at"`
}

type IdentityRelationships struct {
	Account resources.Relation `json:"account"`
}
