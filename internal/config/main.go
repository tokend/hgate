package config

import (
	"net/url"

	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/copus"
	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/tokend/connectors/keyer"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/connectors/submit"
)

var HGateRelease string

type Config interface {
	comfig.Logger
	comfig.Listenerer
	types.Copuser

	keyer.Keyer
	submit.Submission
	signed.Clienter

	HorizonURL() *url.URL
}

type config struct {
	comfig.Logger
	comfig.Listenerer
	types.Copuser

	keyer.Keyer
	signed.Clienter
	submit.Submission

	*horizonConfig

	getter kv.Getter
}

func NewConfig(getter kv.Getter) Config {
	return &config{
		getter:        getter,
		Logger:        comfig.NewLogger(getter, comfig.LoggerOpts{Release: HGateRelease}),
		Listenerer:    comfig.NewListenerer(getter),
		Keyer:         keyer.NewKeyer(getter),
		Submission:    submit.NewSubmission(getter),
		Clienter:      signed.NewClienter(getter),
		Copuser:       copus.NewCopuser(getter),
		horizonConfig: NewHorizon(getter),
	}
}
