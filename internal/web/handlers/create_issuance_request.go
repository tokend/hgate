package handlers

import (
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/tokend/go/strkey"

	"github.com/tokend/hgate/internal/lorem"

	"github.com/tokend/hgate/internal/web/requests"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/tokend/go/xdrbuild"
)

func CreateIssuanceRequest(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateIssuanceRequestRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	receiver, err := getDestinationBalance(request.Asset, request.Receiver, r)
	if err != nil {
		Log(r).WithError(err).Error("failed to get destination balance")
		// TODO: Check not found errors
		ape.RenderErr(w, problems.InternalError())
		return
	}

	op := xdrbuild.CreateIssuanceRequest{
		Asset:    request.Asset,
		Amount:   uint64(request.Amount),
		AllTasks: request.AllTasks,
		Receiver: receiver,
		Details:  request.CreatorDetails,
	}

	if request.Reference == nil {
		op.Reference = lorem.RandomString(lorem.DictionaryAlphaNum, 12)
	} else {
		op.Reference = *request.Reference
	}

	if request.CreatorDetails == nil {
		op.Details = emptyDetails{}
	}

	env, err := buildAndSign(r, &op)

	if err != nil {
		Log(r).WithError(err).Error("failed to marshal transaction")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	proxyTransaction(r, w, env)
}

func getDestinationBalance(asset string, destination string, request *http.Request) (string, error) {
	if _, err := strkey.Decode(strkey.VersionByteBalanceID, destination); err == nil {
		return destination, nil
	}

	if _, err := strkey.Decode(strkey.VersionByteAccountID, destination); err == nil {
		return HorizonConnector(request).GetDefaultBalanceID(asset, destination)
	}

	accountId, err := HorizonConnector(request).GetAccountIDByIdentifier(destination)
	if err != nil {
		return "", errors.Wrap(err, "failed to get account id by identifier")
	}
	return HorizonConnector(request).GetDefaultBalanceID(asset, accountId)
}

type emptyDetails struct{}

func (d emptyDetails) MarshalJSON() ([]byte, error) {
	return []byte("{}"), nil
}
