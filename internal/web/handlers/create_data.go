package handlers

import (
	"github.com/tokend/hgate/internal/web/requests"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/tokend/go/xdrbuild"
	"net/http"
)

func CreateData(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateDataRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	env, err := buildAndSign(r, &xdrbuild.CreateData{
		Type: request.Type,
		Value: request.Value,
	})

	if err != nil {
		Log(r).WithError(err).Error("failed to marshal transaction")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	proxyTransaction(r, w, env)
}