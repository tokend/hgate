package handlers

import (
	"context"
	"net/http"

	"github.com/tokend/hgate/internal/helpers"
	"github.com/tokend/hgate/internal/horizon"
	"gitlab.com/tokend/connectors/submit"
	oldkeypair "gitlab.com/tokend/go/keypair"
	"gitlab.com/tokend/go/signcontrol"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	builderCtxKey
	keysCtxKey
	submitterCtxKey
	proxyCtxKey
	horizonConnectorCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxBuilder(builder *xdrbuild.Builder) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, builderCtxKey, builder)
	}
}

func Builder(r *http.Request) *xdrbuild.Builder {
	return r.Context().Value(builderCtxKey).(*xdrbuild.Builder)
}

func CtxKeys(keys SigningBundle) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, keysCtxKey, keys)
	}
}

func Keys(r *http.Request) SigningBundle {
	return r.Context().Value(keysCtxKey).(SigningBundle)
}

func CtxSubmitter(transactor *submit.Submitter) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, submitterCtxKey, transactor)
	}
}

func Submitter(r *http.Request) *submit.Submitter {
	return r.Context().Value(submitterCtxKey).(*submit.Submitter)
}

func signRequest(r *http.Request, signer keypair.Full) error {
	return signcontrol.SignRequest(r, oldkeypair.MustParse(signer.Seed()))
}

func CtxProxy(proxy helpers.Proxy) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, proxyCtxKey, proxy)
	}
}

func Proxy(w http.ResponseWriter, r *http.Request) {
	proxy := r.Context().Value(proxyCtxKey).(helpers.Proxy)
	proxy(w, r)
}

func CtxHorizonConnector(transactor *horizon.Connector) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, horizonConnectorCtxKey, transactor)
	}
}

func HorizonConnector(r *http.Request) *horizon.Connector {
	return r.Context().Value(horizonConnectorCtxKey).(*horizon.Connector)
}
