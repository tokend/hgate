package requests

import (
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/tokend/hgate/resources"
	"net/http"
)

func NewCreateDataRequest(r *http.Request) (resources.CreateData, error) {
	var d dummy
	var request resources.CreateData
	if err := json.NewDecoder(r.Body).Decode(&d); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	if err := json.Unmarshal(d.Data, &request); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal request")
	}

	return request, nil
}


