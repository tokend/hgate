package requests

import (
	"encoding/json"
	"github.com/tokend/hgate/resources"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
)

func NewCreateIssuanceRequestRequest(r *http.Request) (resources.CreateIssuanceRequest, error) {
	var d dummy
	var request resources.CreateIssuanceRequest
	if err := json.NewDecoder(r.Body).Decode(&d); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	if err := json.Unmarshal(d.Data, &request); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal request")
	}

	return request, nil
}
