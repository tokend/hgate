package lorem

import (
	"math/rand"
	"time"
)

const (
	DictionaryAlphaNum      = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	DictionaryUpperAlphaNum = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	DictionaryNum           = "1234567890"
)

func RandomString(dictionary string, digits int) string {
	rand.Seed(time.Now().Unix())
	b := make([]byte, digits)
	for i := range b {
		b[i] = dictionary[rand.Intn(len(dictionary))]
	}
	return string(b)
}
